package crypto

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"

	log "gitlab.com/purtonen/go-auth/logging"
)

var bitSize = 4096

type keyData struct {
	PrivateKeyHex string `json:"privateKeyHex"`
	PublicKeyHex  string `json:"publicKeyHex"`
	PrivateKeyPem string `json:"privateKeyPem"`
	PublicKeyPem  string `json:"publicKeyPem"`
}

// GenerateKeyPair : generate and return an RSA private key
func GenerateKeyPair() (*rsa.PrivateKey, error) {
	return rsa.GenerateKey(rand.Reader, bitSize)
}

// PublicKeyToString : Convert public key to PEM string
func PublicKeyToString(key *rsa.PublicKey) string {
	keyString, err := x509.MarshalPKIXPublicKey(key)
	log.CheckError(err)

	return string(pem.EncodeToMemory(&pem.Block{
		Type:  "RSA PRIVATE KEY",
		Bytes: keyString,
	}))
}

// PrivateKeyToString : Convert public key to PEM string
func PrivateKeyToString(key *rsa.PrivateKey) string {
	return string(pem.EncodeToMemory(&pem.Block{
		Type:  "RSA PRIVATE KEY",
		Bytes: x509.MarshalPKCS1PrivateKey(key),
	}))
}

// StringToKeyPair : create a rsa.PrivateKEy object from key string
func StringToKeyPair(keyString string) (*rsa.PrivateKey, error) {
	block, _ := pem.Decode([]byte(keyString))
	enc := x509.IsEncryptedPEMBlock(block)
	b := block.Bytes
	var err error

	if enc {
		b, err = x509.DecryptPEMBlock(block, nil)
		log.CheckError(err)
	}
	key, err := x509.ParsePKCS1PrivateKey(b)
	log.CheckError(err)

	return key, err
}

// // GetSessionKeys : returns the necessary public keys
// func GetSessionKeys(sessionID uuid.UUID) {

// }
