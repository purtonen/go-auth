package crypto

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha512"
)

// EncryptMessage : encrypts the specified message with the specified public key
func EncryptMessage(message string, pubKey *rsa.PublicKey) ([]byte, error) {
	hash := sha512.New()
	bytes, err := rsa.EncryptOAEP(hash, rand.Reader, pubKey, []byte(message), nil)
	return bytes, err
}

// DecryptMessage : decrypts the specified message with the specified private key
func DecryptMessage(cipher []byte, privKey *rsa.PrivateKey) (string, error) {
	hash := sha512.New()
	bytes, err := rsa.DecryptOAEP(hash, rand.Reader, privKey, cipher, nil)
	return string(bytes), err
}
