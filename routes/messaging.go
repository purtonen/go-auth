package routes

import (
	"net/url"

	"gitlab.com/purtonen/go-auth/crypto"
)

type keyData struct {
	PrivateKeyHex string `json:"privateKeyHex"`
	PublicKeyHex  string `json:"publicKeyHex"`
	PrivateKeyPem string `json:"privateKeyPem"`
	PublicKeyPem  string `json:"publicKeyPem"`
}

func init() {
	addRoute("/testCrypt", "GET", "scope", testCrypt)
}

func testCrypt(vars urlVariables, params url.Values, body jsonStruct) response {
	msg := "Testiviesti on tässä mitä kryptataan"
	key, _ := crypto.GenerateKeyPair()
	encrypted, _ := crypto.EncryptMessage(msg, &key.PublicKey)

	keyString := crypto.PrivateKeyToString(key)
	regeneratedKey, _ := crypto.StringToKeyPair(keyString)
	decrypted, _ := crypto.DecryptMessage(encrypted, regeneratedKey)

	return successResponse(decrypted)
}
