package routes

import (
	"net/url"

	log "gitlab.com/purtonen/go-auth/logging"
)

type testStruct struct {
	Name        string      `json:"name"`
	InnerStruct innerStruct `json:"innerStruct"`
}
type innerStruct struct {
	Prop      string `json:"prop"`
	Number    int    `json:"number"`
	IsBoolean bool   `json:"isBool"`
}

func init() {
	addRoute("/endpoint", "GET", "scope", test)
	addRoute("/endpoint2", "GET", "scope", test)
}

func test(vars urlVariables, params url.Values, body jsonStruct) response {
	log.Infoln("Fatal")
	log.Traceln("Trace")
	log.Errorln("Error")
	log.Warnignln("Warnign")
	testRes := testStruct{
		Name: "Eetu Purtonen",
		InnerStruct: innerStruct{
			Prop:      "Some property value",
			Number:    9001,
			IsBoolean: true,
		},
	}
	return successResponse(testRes)
}
