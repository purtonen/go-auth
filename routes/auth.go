package routes

import (
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"net/url"

	"gitlab.com/purtonen/go-auth/crypto"
	log "gitlab.com/purtonen/go-auth/logging"
)

func init() {
	log.Infoln("init auth")

	addRoute("/getPrivateKey", "POST", "scope", getPrivateKey)
}

func getPrivateKey(vars urlVariables, params url.Values, body jsonStruct) response {
	log.Info(vars)
	log.Info(params)
	log.Info(body)
	key, _ := crypto.GenerateKeyPair()
	pubBytes, _ := x509.MarshalPKIXPublicKey(&key.PublicKey)
	keyData := keyData{
		PrivateKeyHex: fmt.Sprintf("%x", key.D.Bytes()),
		PublicKeyHex:  fmt.Sprintf("%x", key.PublicKey.N.Bytes()),
		PrivateKeyPem: string(pem.EncodeToMemory(&pem.Block{
			Type:  "RSA PRIVATE KEY",
			Bytes: x509.MarshalPKCS1PrivateKey(key),
		})),
		PublicKeyPem: string(pem.EncodeToMemory(&pem.Block{
			Type:  "RSA PUBLIC KEY",
			Bytes: pubBytes,
		})),
	}

	return successResponse(keyData)
}
