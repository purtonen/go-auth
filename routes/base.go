package routes

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"

	"github.com/gorilla/mux"

	log "gitlab.com/purtonen/go-auth/logging"
)

var routes []route
var router *mux.Router

type response struct {
	Code  int         `json:"code"`
	Error string      `json:"error"`
	Data  interface{} `json:"data"`
}

type urlVariables map[string]string
type jsonStruct map[string]interface{}

func errorResponse(code int, errorMsg string) response {
	return response{Code: code, Error: errorMsg}
}

func successResponse(data interface{}) response {
	return response{Code: 0, Data: data}
}

type callback func(urlVariables, url.Values, jsonStruct) response

type route struct {
	url         string
	method      string
	scope       string
	callback    callback
	isSensitive bool
}

func (route route) equals(otherRoute route) bool {
	return route.url == otherRoute.url && route.method == otherRoute.method
}

const (
	baseURL = "/api"
)

func init() {
	fmt.Println("routes init")
	router = mux.NewRouter()
}

func addRoute(url string, method string, scope string, fn callback) error {
	// Create new route object
	newRoute := route{
		url:      baseURL + url,
		method:   method,
		scope:    scope,
		callback: fn,
	}

	// Go through existing routes and makes sure they don't collide
	for _, route := range routes {
		if route.equals(newRoute) {
			log.Errorf("Route %s %s already exists\n", newRoute.method, newRoute.url)
			return errors.New("Route already exists")
		}
	}
	routes = append(routes, newRoute)
	return nil
}

// StartListening : Start the actual HTTP listeners
func StartListening() {
	for _, route := range routes {
		router.HandleFunc(route.url, CallbackHandler).Methods(route.method)
	}
	
	// TODO: create multiple subrouters and add different middlewares to them

	// Add middlewares
	router.Use(accessLogging, cbh)

	// Trailing slashes need to be removed before handling anything, Middleware won't work
	log.Fatal(http.ListenAndServe(":8000", removeTrailingSlash(router)))
}

func removeTrailingSlash(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Info("remove trailing slash done here")
		r.URL.Path = strings.TrimSuffix(r.URL.Path, "/")
		next.ServeHTTP(w, r)
	})
}

func accessLogging(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// TODO: Move Logging logic here
		log.Tracef("%s %s", r.Method, r.URL.Path)
		log.Info("LOGGING MIDDLEWARE RUN")
		next.ServeHTTP(w, r)
		log.Info("LOGGING MIDDLEWARE FINISH")
	})
}

func cbh(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// TODO: Move callback function handling logic here
		log.Info("CBH RUN")
		next.ServeHTTP(w, r)
		log.Info("CBH FINISH")
	})
}

// TODO: Remove this ugly handler function. it's not working properly!
// Consider calling the callback function as normal, but return a handler function instead
// CallbackHandler : handle incoming traffic and route it to the correct functions
func CallbackHandler(w http.ResponseWriter, r *http.Request) {
	log.Info("CALLBACKHANDLER RUN")

	// Set response type to JSON
	w.Header().Set("Content-Type", "application/json")
	var callback callback
	vars := mux.Vars(r)
	params := r.URL.Query()
	body, err := ioutil.ReadAll(r.Body)
	log.CheckError(err)
	shouldLog := false

	// Undefined json structure
	var bodyObject jsonStruct
	err = json.Unmarshal(body, &bodyObject)

	// Set default error response
	response := response{Code: -1, Error: "route mismatch"}

	// Look for the specified route
	for _, route := range routes {
		if r.Method == route.method && r.URL.Path == route.url {
			callback = route.callback
			// Sensitive routes should not be logged, eg. password requests
			shouldLog = !route.isSensitive
		}
	}

	// Call the route callback function if it was found
	if callback != nil {
		response = callback(vars, params, bodyObject)
	}

	// Marshal the callback or default error response to JSON
	resJSON, err := json.Marshal(response)
	log.CheckError(err)

	// Log all requests except GET and auth requests
	if r.Method != "GET" && shouldLog {
		reqJSON, _ := json.Marshal(vars)
		log.Tracef("%s %s REQUEST: %s RESPONSE: %s", r.Method, r.URL.Path, string(reqJSON), string(resJSON))
	}

	// Finally write the response JSON back to the HTTP ResponseWriter
	w.Write(resJSON)
}
