package models

// User : reflects a user found in the database
type User struct {
	UUID      string `json:"UUID"`
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
}
