package db

import (
	"context"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"

	log "gitlab.com/purtonen/go-auth/logging"
)

var client *mongo.Client
var ctx context.Context

// InitDB : Initialize database connection
func InitDB(dataSourceName string) {
	var err error

	log.Info(dataSourceName)
	// Create mongodb client
	client, err = mongo.NewClient(options.Client().ApplyURI(dataSourceName))
	log.CheckError(err)

	// Connect to the database
	ctx, _ = context.WithTimeout(context.Background(), 10*time.Second)
	err = client.Connect(ctx)
	log.CheckError(err)

	// Ping the server to check it was found
	ctx, _ = context.WithTimeout(context.Background(), 5*time.Second)
	err = client.Ping(ctx, readpref.Primary())
	log.CheckError(err)

	collection := client.Database("auth").Collection("users")
	ctx, _ = context.WithTimeout(context.Background(), 5*time.Second)
	res, err := collection.InsertOne(ctx, bson.M{"name": "pi", "value": 3.14159})
	log.CheckError(err)

	log.Info(res.InsertedID)
}

func pageToOffset(page int, limit int) int {
	if page == 0 || page == 1 {
		return 0
	}

	return (page - 1) * limit
}
