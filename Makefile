default: build
	@echo "============= Starting Go-Auth locally ============="
	docker-compose up -d
	
build:
	@echo "============= Building Go-Auth ============="
	docker build --rm -t go-auth .

logs:
	docker-compose logs -f

down:
	docker-compose down

test:
	go test -v -cover ./...

clean: down
	@echo "=============cleaning up============="
	rm -f api
	docker system prune -f
	docker volume prune -f