FROM golang

WORKDIR /go/src/gitlab.com/purtonen/go-auth
COPY . .

RUN go get -d -v
RUN go install -v

CMD ["go-auth"]