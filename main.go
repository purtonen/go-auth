package main

import (
	"fmt"
	"net/url"
	"os"

	db "gitlab.com/purtonen/go-auth/database"
	log "gitlab.com/purtonen/go-auth/logging"
	"gitlab.com/purtonen/go-auth/routes"
)

func init() {
	log.Info("main init")
}

func main() {
	log.Info("main main")
	mongoUser := os.Getenv("MONGO_USER")
	mongoPass := url.QueryEscape(os.Getenv("MONGO_PASSWORD"))
	mongoHost := os.Getenv("MONGO_HOST")
	mongoPort := os.Getenv("MONGO_PORT")
	//mongoDatabase := "admin" //os.Getenv("MONGO_DATABASE")
	db.InitDB(fmt.Sprintf("mongodb://%s:%s@%s:%s", mongoUser, mongoPass, mongoHost, mongoPort))
	routes.StartListening()
}
